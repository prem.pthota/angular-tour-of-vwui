import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, filter, take,concatMap,finalize } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AuthService } from 'src/app/modules/auth/_services/auth-service/auth-service.service';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  isRefreshingToken = false;

  tokenRefreshed$ = new BehaviorSubject<boolean>(false);

  urlsToNotUse: Array<string>;
//;
  constructor(private authService:AuthService) {
	    this.urlsToNotUse= [
      `${environment.moduleURI}/authorize`,
      `${environment.moduleURI}/refresh`
    ];
  
}
  addToken(req: HttpRequest<any>): HttpRequest<any> {
	if (this.isValidRequestForInterceptor(req.url)) {
    const token = this.authService.getJwtToken();
    //console.info('Token added successfully to request');
    return token ? req.clone({ setHeaders: { Authorization: 'Bearer ' + token } }) : req;
   }else{
	return req;
}
  }

  private isValidRequestForInterceptor(requestUrl: string): boolean {
	let val=true;
      for (let address of this.urlsToNotUse) {
        if (address==requestUrl) {
	          val= false;
        }      
    }
    return val;
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.addToken(req)).pipe(
      catchError(err => {
        if (err.status === 403 && this.isValidRequestForInterceptor(req.url)) {
	     return this.handle401Error(req, next);
        }
        return throwError(err);
      })
    );
  }

  private handle401Error(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (this.isRefreshingToken) {
      return this.tokenRefreshed$.pipe(
        filter(Boolean),
        take(1),
        concatMap(() => next.handle(this.addToken(req)))
      );
    }

    this.isRefreshingToken = true;
   this.tokenRefreshed$.next(false);
    return this.authService.refreshToken().pipe(
      concatMap((res: any) => {
          this.tokenRefreshed$.next(true);
          return next.handle(this.addToken(req));
      }),
      catchError((err) => {
        this.authService.logout();
        return throwError(err);
      }),
      finalize(() => {
        this.isRefreshingToken = false;
      })
    );
  }
}