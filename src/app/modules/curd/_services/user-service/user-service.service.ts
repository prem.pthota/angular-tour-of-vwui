import { Injectable } from '@angular/core';
import { catchError, throwError,Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { UserHttpService } from '../user-http/user-http.service';
import { User } from '../../_models/user.model';
@Injectable({
	providedIn: 'root'
})
export class UserService {

	constructor(private userHttpService: UserHttpService) { }

	saveUser(user: User): Observable<User>  {
		return this.userHttpService.saveUser(user).pipe(catchError(this.handleError));
	}
	updateUser(user: User): Observable<User> {
		return this.userHttpService.updateUser(user).pipe(catchError(this.handleError));
	}

	getUser(id: BigInteger): Observable<User> {
		return this.userHttpService.getUser(id).pipe(catchError(this.handleError));
	}
	getAllUsers(): Observable<User> {
		return this.userHttpService.getAllUsers().pipe(catchError(this.handleError));
	}
	deleteUser(id: BigInteger): Observable<User> {
		return this.userHttpService.deleteUser(id).pipe(catchError(this.handleError));
	}
	deleteUsersWithIds(userIds:[]): Observable<any> {
		return this.userHttpService.deleteUsersWithIds(userIds).pipe(catchError(this.handleError));
	  }
	private handleError(error: HttpErrorResponse) {
		let errorMessage = '';
		if (error.error instanceof ErrorEvent) {
			errorMessage = `Error: ${error.error.message}`;
		} else {
			errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
		}
		return throwError(errorMessage);
	};
}
