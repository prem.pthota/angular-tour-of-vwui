import { Injectable } from '@angular/core';
import { throwError, catchError,Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';
import { User } from '../../_models/user.model';

const API_USER_URL = `${environment.moduleURI}/user`;
@Injectable({
  providedIn: 'root'
})
export class UserHttpService {


  constructor(private http: HttpClient, private router: Router) { }

  saveUser(user: User): Observable<User> {
    return this.http.post<User>(`${API_USER_URL}/save`, user).pipe(catchError(this.handleError));
  }
  updateUser(user: User): Observable<User> {
    return this.http.put<User>(`${API_USER_URL}/update/${user.id}`, user).pipe(catchError(this.handleError));
  }

  getUser(id: BigInteger): Observable<User> {
    return this.http.get<User>(`${API_USER_URL}/get/${id}`).pipe(catchError(this.handleError));
  }
  getAllUsers(): Observable<User> {
    return this.http.get<User>(`${API_USER_URL}/get/all`).pipe(catchError(this.handleError));
  }
  deleteUser(id: BigInteger): Observable<User> {
    return this.http.delete<User>(`${API_USER_URL}/delete/${id}`).pipe(catchError(this.handleError));
  }

  deleteUsersWithIds(userIds:[]): Observable<any> {
    return this.http.delete<any>(`${API_USER_URL}/delete/ids`,{'body':userIds}).pipe(catchError(this.handleError));
  }


  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.status === 502 || error.status === 0 || error.status === -1) {
			this.router.navigateByUrl('/error/500');
		}
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  };
  
}
