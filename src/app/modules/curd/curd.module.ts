import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CurdRoutingModule } from './curd-routing.module';
import { CurdComponent } from './curd.component';
import { UsersComponent } from './users/users.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VwuiAngularModule } from '@recognizebv/vwui-angular';


@NgModule({
  declarations: [
    CurdComponent,
    UsersComponent,
  ],
  imports: [
    CommonModule,
    CurdRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    VwuiAngularModule.forRoot(),
  ]
})
export class CurdModule { }
