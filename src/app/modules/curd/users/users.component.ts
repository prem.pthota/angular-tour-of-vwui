import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgToastService } from 'ng-angular-popup';
import { Subscription } from 'rxjs';
import { User } from '../_models/user.model';
import { UserService } from '../_services/user-service/user-service.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {

  userFrom: FormGroup;
  submitted:boolean = false;
  public unsubscribe: Subscription[] = [];
  public userIds: any = [];
  public userList: User;
  showHideModal: boolean = false;
  modalTitle: string = ""
  constructor(private userService: UserService, private fb: FormBuilder, private toastr: NgToastService) { }

  ngOnInit(): void {
    this.initList();
  }

  initList() {
    const listSubscr = this.userService.getAllUsers().subscribe((res: User) => {
      this.userList = res;
      this.showHideModal = false
    })

    this.unsubscribe.push(listSubscr);

    this.userFrom = this.fb.group({
      id:[0],
      name: ['', Validators.required],      
      loginId: ['', Validators.required,Validators.email],
      password: ['',Validators.required],
      token:['']
    })
  }
  get f() {
    return this.userFrom.controls;
  }

  selectedData(id, event) {
    if (event.target.tagName == 'INPUT') {
      if (this.userIds.find(res => res === id)) {
        this.userIds = this.userIds.filter(res => res !== id)
      } else {
        this.userIds.push(id)
      }
    }
  }

  add(mode: string) {
    this.showHideModal = true;
    this.modalTitle = mode;
  }

  edit(mode: string) {
    if (this.userIds.length) {
      if (this.userIds.length == 1) {
        this.showHideModal = true;
        this.modalTitle = mode
        const [id] = this.userIds
        const editSubscr = this.userService.getUser(id).subscribe(res => {
          this.userFrom.setValue(res)
        })
        this.unsubscribe.push(editSubscr);
      } else {
        this.toastr.error({ detail: "Error", summary: 'Check Only One Checkbox' })
      }
    } else {
      this.toastr.error({ detail: "Error", summary: 'Check Atleast One Checkbox' })
    }

  }
  delete(mode: string) {
    if (this.userIds.length) {
      this.showHideModal = true;
      this.modalTitle = mode
    } else {
      this.toastr.error({ detail: "Error", summary: 'Check Atleast One Checkbox' })
    }
  }

  deleteData() {
    const deleteSubscr = this.userService.deleteUsersWithIds(this.userIds).subscribe(res => {
      this.clearCache();
      this.toastr.success({ detail: "Success", summary: 'User List deleted successful' })
    })
    this.unsubscribe.push(deleteSubscr);
  }

  onSubmit() {
    this.submitted = false;
    if (this.userFrom.invalid) {
      this.submitted = true;
    }
    else {
      if (!this.userFrom.get('id')?.value) {
        const updateSubscr = this.userService.saveUser(this.userFrom.value).subscribe(res => {
          this.clearCache()
          this.toastr.success({ detail: "Success", summary: 'User Save successful' })
        }, (error) => {
          this.toastr.error({ detail: "Error", summary: 'Email already exists' })
        })
        this.unsubscribe.push(updateSubscr);
      } else {
        const saveSubscr = this.userService.updateUser(this.userFrom.value).subscribe(res => {
          this.clearCache()
          this.toastr.success({ detail: "Success", summary: 'User Update successful' })
        }, (error) => {
          this.toastr.error({ detail: "Error", summary: 'Email already exists' })
        })
        this.unsubscribe.push(saveSubscr);
      }
    }
  }

  cancel() {
    this.showHideModal = false;
    this.submitted = false;
    this.userFrom.reset();
  }

  clearCache() {
    this.userIds=[];
    this.userFrom.reset();
    this.showHideModal = false;
    this.submitted = false;
    this.initList();
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }
}
