export interface User {
    id: BigInteger
    name: string
    password: string
    loginId: string
    token: string
}
