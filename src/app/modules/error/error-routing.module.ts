import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/_services/auth-guard/auth.guard';
import { ErrorComponent } from './error.component';
import { Error1Component } from './error1/error1.component';
import { Error2Component } from './error2/error2.component';
import { Error3Component } from './error3/error3.component';
import { Error4Component } from './error4/error4.component';
import { Error5Component } from './error5/error5.component';
import { Error6Component } from './error6/error6.component';

const routes: Routes = [
  {
    path: '',
    component: ErrorComponent,
    children: [
      {
        path: '403',
        component: Error1Component,
      },
      {
        path: '404',
        component: Error2Component,
      },
      {
        path: '401',
        component: Error3Component
      },
      {
        path: '304',
        component: Error4Component,
      },
      {
        path: '500',
        component: Error5Component,
      },
      {
        path: '503',
        component: Error6Component,
      },
      { path: '', redirectTo: '404', pathMatch: 'full' },
      {
        path: '**',
        component: Error2Component,
        pathMatch: 'full',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorRoutingModule { }
