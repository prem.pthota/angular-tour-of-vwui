import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../_services/auth-service/auth-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  private unsubscribe: Subscription[] = [];
  loginForm: FormGroup;
  loginValidation:boolean= false;
  hasError:boolean= false;
  isLoading$: Observable<boolean>;
  constructor(private authService: AuthService, private router: Router, private fb: FormBuilder) {
    this.isLoading$ = this.authService.isLoading$;
		if (this.authService.currentUserValue) {
			this.router.navigate(['/curd/users']);
		}
  }
  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    })
  }
  get f() {
    return this.loginForm.controls;
  }
  
  login() {
    this.loginValidation = false;
    this.hasError = false;
    if (this.loginForm.invalid) {
      this.loginValidation = true;
    } else {
      const loginSubscr = this.authService.login(this.loginForm.value).subscribe(res => {
        if (res) {
          const authData = this.authService.getAuth();
          if(authData){
          this.router.navigate(['/curd/users']);
        }
        }
        else{
          this.hasError = true;
        }
      }, (error) => {
        this.hasError = true;
      })
      this.unsubscribe.push(loginSubscr);
    }
  }
  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

}
