import { Injectable } from '@angular/core';
import { Observable, of, catchError, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';
import { LoginRequest } from '../../_models/login-request.model';
import { AuthResponse } from '../../_models/auth-response.model';
import { UserResponse } from '../../_models/user-response.model';

const API_AUTH_URL = `${environment.moduleURI}/auth`;
@Injectable({
  providedIn: 'root'
})
export class AuthHttpService {

  constructor(private http: HttpClient, private router: Router) { }

  login(loginRequest: LoginRequest): Observable<AuthResponse> {
    const httpHeaders = new HttpHeaders({
      'Authorization': `${loginRequest.username}`,
    });
    return this.http.post<AuthResponse>(`${API_AUTH_URL}/authorize`, loginRequest, { headers: httpHeaders, withCredentials: true }).pipe(catchError(this.handleError));
  }
  loginWithToken(accessToken: string): Observable<AuthResponse> {
    const httpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${accessToken}`,
    });
    return this.http.get<AuthResponse>(`${API_AUTH_URL}/authorizeWithJWT/${accessToken}`, { headers: httpHeaders, withCredentials: true }).pipe(catchError(this.handleError));
  }

  refreshToken(token: string): Observable<AuthResponse> {
    const httpHeaders = new HttpHeaders({
      Authorization: `Bearer ${token}`,
    });
    return this.http.get<AuthResponse>(`${API_AUTH_URL}/refresh`, { headers: httpHeaders, withCredentials: true }).pipe(catchError(this.handleError));
  }


  logout(): Observable<any> {
    const httpHeaders = new HttpHeaders({
      Authorization: `Bearer logout`,
    });
    return this.http.get<any>(`${API_AUTH_URL}/logout`, { headers: httpHeaders, withCredentials: true }).pipe(catchError(this.handleError));
  }
  getUserByToken(userResponse: UserResponse): Observable<UserResponse> {
    if (!userResponse.user) {
      return this.http.get<UserResponse>(`${API_AUTH_URL}/me`).pipe(catchError(this.handleError));
    } else {
      return of(userResponse).pipe(catchError(this.handleError));
    }
  }
  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.status === 502 || error.status === 0 || error.status === -1) {
			this.router.navigateByUrl('/error/500');
		}
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  };
}
