import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, throwError, map, switchMap, catchError, finalize, Subscription, of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { AuthHttpService } from '../auth-http/auth-http.service';
import { LoginRequest } from '../../_models/login-request.model';
import { AuthResponse } from '../../_models/auth-response.model';
import { UserResponse } from '../../_models/user-response.model';
import { User } from 'src/app/modules/curd/_models/user.model';
@Injectable({
	providedIn: 'root'
})
export class AuthService implements OnDestroy {

	private unsubscribe: Subscription[] = [];
	private authLocalStorageToken = `${environment.localStorageAuthKey}`;
	private userLocalStorageToken = `${environment.localStorageUserKey}`;

	currentUser$: Observable<User>;
	isLoading$: Observable<boolean>;
	currentUserSubject: BehaviorSubject<User>;
	isLoadingSubject: BehaviorSubject<boolean>;

	get currentUserValue(): User {
		return this.currentUserSubject.value;
	}

	set currentUserValue(user: User) {
		this.currentUserSubject.next(user);
	}

	constructor(private authHttpService: AuthHttpService) {
		this.isLoadingSubject = new BehaviorSubject<boolean>(false);
		this.currentUserSubject = new BehaviorSubject<User>(undefined);
		this.currentUser$ = this.currentUserSubject.asObservable();
		this.isLoading$ = this.isLoadingSubject.asObservable();
		const subscr = this.getUserByToken().subscribe();
		this.unsubscribe.push(subscr);
	}

	login(loginRequest: LoginRequest): Observable<User> {
		this.isLoadingSubject.next(true);
		return this.authHttpService.login(loginRequest).pipe(
			map((auth: AuthResponse) => {
				const result = this.setAuthFromLocalStorage(auth);
				return result;
			}),
			switchMap(() => this.getUserByToken()),
			catchError((err) => {
				return of(undefined);
			}),
			finalize(() => this.isLoadingSubject.next(false))
		);
	}
	getAuth(): AuthResponse {
		try {
			const authData = JSON.parse(
				localStorage.getItem(this.authLocalStorageToken)
			);
			return authData;
		} catch (error) {
			return undefined;
		}
	}
	loginWithToken(accessToken: string): Observable<User> {
		this.isLoadingSubject.next(true);
		return this.authHttpService.loginWithToken(accessToken).pipe(
			map((auth: AuthResponse) => {
				const result = this.setAuthFromLocalStorage(auth);
				return result;
			}),
			switchMap(() => this.getUserByToken()),
			catchError((err) => {
				return of(undefined);
			}),
			finalize(() => this.isLoadingSubject.next(false))
		);
	}

	refreshToken(): Observable<User> {
		return this.authHttpService.refreshToken(this.getJwtToken()).pipe(
			map((auth: AuthResponse) => {
				const result = this.setAuthFromLocalStorage(auth);
				return result;
			}),
			switchMap(() => this.getUserByToken()),
			catchError((err) => {
				return of(undefined);
			}),
			finalize(() => this.isLoadingSubject.next(false))
		);
	}

	getUserByToken(): Observable<User> {
		const auth = this.getAuthFromLocalStorage();
		if (!auth || !auth.token) {
			return of(undefined);
		}
		const userResponse: UserResponse = <UserResponse>{};
		userResponse.user = this.getUserFromLocalStorage();
		this.isLoadingSubject.next(true);
		return this.authHttpService.getUserByToken(userResponse).pipe(
			map((userConf: UserResponse) => {
				if (userConf.user) {
					this.currentUserSubject = new BehaviorSubject<User>(userConf.user);
					this.setUserInLocalStorage(userConf.user);
				} else {
					this.logout();
				}
				return userConf.user;
			}),
			finalize(() => this.isLoadingSubject.next(false))
		);
	}


	logout() {
		this.clearCookies().subscribe(res => {
				localStorage.clear();
				localStorage.setItem("logout", "logout");
				this.loginRedirect();
		})
	}

	loginRedirect() {
		window.location.href = '/auth/login';
	}

	clearCookies(): Observable<any> {
		return this.authHttpService.logout().pipe(catchError(this.handleError));
	}

	getJwtToken(): string {
		try {
			const authData = JSON.parse(
				localStorage.getItem(this.authLocalStorageToken)
			);
			return authData.token;
		} catch (error) {
			return undefined!;
		}
	}
	private setAuthFromLocalStorage(auth: AuthResponse): boolean {
		if (auth && auth.token) {
			localStorage.setItem(this.authLocalStorageToken, JSON.stringify(auth));
			return true;
		}
		return false;
	}

	private getAuthFromLocalStorage(): AuthResponse {
		try {
			const authData = JSON.parse(
				localStorage.getItem(this.authLocalStorageToken)
			);
			return authData;
		} catch (error) {
			return undefined;
		}
	}
	
	private setUserInLocalStorage(user: User): boolean {
		if (user) {
			localStorage.setItem(this.userLocalStorageToken, JSON.stringify(user));
			return true;
		}
		return false;
	}
	private getUserFromLocalStorage(): User {

		try {
			const authData = JSON.parse(
				localStorage.getItem(this.userLocalStorageToken)
			);
			return authData;
		} catch (error) {
			return undefined;
		}
	}
	private handleError(error: HttpErrorResponse) {
		let errorMessage = '';
		if (error.error instanceof ErrorEvent) {
			errorMessage = `Error: ${error.error.message}`;
		} else {
			errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
		}
		return throwError(errorMessage);
	};

	ngOnDestroy() {
		this.unsubscribe.forEach((sb) => sb.unsubscribe());
	}
}
