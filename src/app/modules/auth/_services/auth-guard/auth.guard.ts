import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router} from '@angular/router';
import { AuthService } from '../auth-service/auth-service.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate,CanActivateChild {
  constructor(private authService: AuthService, private router: Router) { }
  canActivate() {
    const currentUser = this.authService.currentUserValue;
    if (currentUser) {
      this.router.navigate(['/curd/users']);
      return true;
    } else {
      this.router.navigate(['/auth/login']);
      this.authService.logout();
      return false;
    }
  }

  canActivateChild() {
    return this.canActivate();
  }
  
}
