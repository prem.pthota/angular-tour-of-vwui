import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../curd/_services/user-service/user-service.service';
import { Subscription } from 'rxjs';
import { NgToastService } from 'ng-angular-popup';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit, OnDestroy {
  private unsubscribe: Subscription[] = [];
  registerForm: FormGroup;
  submitted:boolean = false;
  constructor(private userService: UserService, private fb: FormBuilder,private toastr:NgToastService) { }

  ngOnInit(): void {
    this.initList();
    
  }
  initList(){
    this.registerForm = this.fb.group({
      id:[0],
      name: ['', Validators.required],      
      loginId: ['', Validators.required,Validators.email],
      password: ['',Validators.required],
      token:['']
    })
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = false;
    if (this.registerForm.invalid) {
      this.submitted = true;
    }
    else  {
      const registerSubscr = this.userService.saveUser(this.registerForm.value).subscribe((res) => {
        this.toastr.success({detail:"Success",summary:'Registration successful'})
        this.registerForm.reset();
       }, (error) => {
        this.toastr.error({detail:"Error",summary:'Email already exists'})
       })
      this.unsubscribe.push(registerSubscr);
    }
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

}
