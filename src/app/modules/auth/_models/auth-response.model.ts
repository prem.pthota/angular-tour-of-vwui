export interface AuthResponse {
    id: BigInteger
    token: string
    type: string
}
