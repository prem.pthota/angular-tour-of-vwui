import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from './modules/auth/_services/auth-service/auth-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private unsubscribe: Subscription[] = [];
  isLoggedUser: boolean;
  constructor(private authService: AuthService,private router: Router) {
    this.isLoggedUser = true;
    if (this.authService.currentUserValue) {
			this.isLoggedUser = false;
		}
   
  }
  ngOnInit(): void {
    window.addEventListener('storage', (event) => {
      if (event.storageArea == localStorage) {
        let token = this.authService.getJwtToken();
        if (token == undefined  && localStorage.getItem("logout")) {
          this.authService.logout();
        }
      }
    }, false);

  }
  ngOnDestroy(): void {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }
}
