export const environment = {
  production: true,
  appVersion: 'v1.0',
  localStorageAuthKey: 'app-AUTH',
	localStorageUserKey: 'app-USER',
  moduleURI: 'http://localhost:1010/product-app' ,
};
